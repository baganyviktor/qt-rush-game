#ifndef beadando_H
#define beadando_H

#include <QWidget>
#include <QPushButton>
#include <QGridLayout>
#include <QComboBox>
#include <QLabel>
#include <QMessageBox>
#include <QString>

class beadando : public QWidget
{
    Q_OBJECT

    public:
        beadando(QWidget* parent = 0);

    private slots:
        void ButtonClicked();
        void NewGameButtonClicked();

    private:
        void StartGame();
        void StopGame();
        void StepGame(const int x, const int y);
        void BabuMozgat(const int x, const int y, const int newx, const int newy);
        void ResetPlayers();
        void UpdateGameView();
        void ResetView();
        int getMeret();
        void HideButtons();
        void CheckWin(int id);
        int LegalLepes(const int k, const int l, const int x, const int y);

        int lepesekszama;
        int kijelolt[2] = {};
        bool vankijelolt;
        int playerid;
        int hogy;
        int** gamedata;
        QString jatekuzenet;
        QGridLayout* gombtar;
        QVBoxLayout* foview;
        QComboBox* palyameret;
        QPushButton* newGameButton; // új játék gombja
        QVector<QVector<QPushButton*>> gombdata; // gombtábla
        QMessageBox msgBox;
};

#endif // QUEEN_H
