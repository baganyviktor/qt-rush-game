#include "widget.h"
#include <time.h>

#define PLAYERONE 1
#define PLAYERTWO 2
#define ATLOBAN 1
#define MELLETTE 2
#define HIBAS -1

/*  Készítsünk programot, amellyel a következő kétszemélyes játékot lehet játszani.
    Adott egy n × n mezőből álló tábla, ahol a két játékos bábúi egymással szemben
    helyezkednek el, két sorban (pont, mint egy sakktáblán, így mindkét játékos 2n
    bábuval rendelkezik, ám mindegyik bábu ugyanolyan típusú). A játékos bábúival
    csak előre léphet egyenesen, vagy átlósan egy mezőt (azaz oldalra, és hátra felé
    nem léphet), és hasonlóan ütheti a másik játékos bábúját előre átlósan
    (egyenesen nem támadhat). Az a játékos győz, aki először átér a játéktábla másik
    végére egy bábuval.
    A program biztosítson lehetőséget új játék kezdésére a táblaméret megadásával
    (6 × 6, 8 × 8, 10 × 10), és ismerje fel, ha vége a játéknak. Ekkor jelenítse meg,
    melyik játékos győzött, majd automatikusan kezdjen új játékot. */

beadando::beadando(QWidget *parent) : QWidget(parent)
{
    setMinimumSize(600, 100);
    setBaseSize(600,600);
    setWindowTitle(trUtf8("Áttörés"));

    newGameButton = new QPushButton();
    newGameButton->setText(trUtf8("Új játék kezdése"));
    connect(newGameButton, SIGNAL(clicked()), this, SLOT(NewGameButtonClicked()));

    /*
     * A legördülő menü megkonstruálása
    */
    palyameret = new QComboBox();
    palyameret->addItem("6x6");
    palyameret->addItem("8x8");
    palyameret->addItem("10x10");

    gombtar = new QGridLayout();

    foview = new QVBoxLayout();
    foview->addWidget(palyameret);
    foview->addWidget(newGameButton);

    gamedata = new int*[10];
    gombdata.resize(10);
    for(int i=0; i<10;i++)
    {
        gamedata[i] = new int[10];
        gombdata[i].resize(10);
        for(int j=0; j<10;j++)
        {
            gombdata[i][j] = new QPushButton();
            gombdata[i][j]->setFont(QFont("Times New Roman", 30, QFont::Bold));
            gombdata[i][j]->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
            gombdata[i][j]->hide();
            gombtar->addWidget(gombdata[i][j],i,j);
            connect(gombdata[i][j],SIGNAL(clicked(bool)),this,SLOT(ButtonClicked()));
        }
    }
    foview->addLayout(gombtar);

    setLayout(foview);
}
void beadando::StartGame()
{
    setMinimumSize(600,600);
    palyameret->hide();
    lepesekszama = 0;

    for(int i=0; i<getMeret(); i++)
    {
        for(int j=0; j<getMeret(); j++)
        {
            gombdata[i][j]->show();
        }
    }

    srand (time(NULL));
    playerid = 1+rand()%2;

    vankijelolt = false;

    ResetView();
    ResetPlayers();
    UpdateGameView();
}
void beadando::StopGame()
{
    HideButtons();
    palyameret->show();
}
void beadando::StepGame(const int x, const int y)
{
    if(!vankijelolt)
    {
        if(gamedata[x][y] == playerid)
        {
            kijelolt[0] = x;
            kijelolt[1] = y;
            vankijelolt = true;
        }
    }
    else
    {
        hogy = LegalLepes(kijelolt[0],kijelolt[1],x,y);
        if(hogy != HIBAS)
        {
            if(gamedata[x][y] != 0)
            {
                if(hogy == ATLOBAN)
                {
                    if(playerid != gamedata[x][y])
                    {
                        BabuMozgat(kijelolt[0],kijelolt[1],x,y);
                        lepesekszama++;
                        if(playerid == PLAYERONE)
                            playerid = PLAYERTWO;
                        else
                            playerid = PLAYERONE;
                        vankijelolt = false;
                    }
                }
            }
            else
            {
                BabuMozgat(kijelolt[0],kijelolt[1],x,y);
                lepesekszama++;
                if(playerid == PLAYERONE)
                    playerid = PLAYERTWO;
                else
                    playerid = PLAYERONE;
                vankijelolt = false;
            }
        }
        if(kijelolt[0] == x && kijelolt[1] == y)
            vankijelolt = false;
    }
}
void beadando::BabuMozgat(const int x, const int y, const int newx, const int newy)
{
    gamedata[newx][newy] = gamedata[x][y];
    gamedata[x][y] = 0;
    if(newy == 0 || newy == getMeret()-1) CheckWin(gamedata[newx][newy]);
}
void beadando::ResetPlayers()
{
    for(int i=0; i<getMeret(); ++i)
    {
        for(int j=0; j<2; ++j)
        {
            gamedata[i][j] = PLAYERONE;
        }
    }
    for(int i=0; i<getMeret(); ++i)
    {
        for(int j=getMeret()-2; j<getMeret(); ++j)
        {
            gamedata[i][j] = PLAYERTWO;
        }
    }
}
int beadando::getMeret()
{
    switch (palyameret->currentIndex()) {
    case 0:
        return 6;
        break;
    case 1:
        return 8;
    case 2:
        return 10;
    default:
        return 6;
        break;
    }
}
int beadando::LegalLepes(const int k, const int l, const int x, const int y)
{
    if(gamedata[k][l] != playerid) return HIBAS;
    if(playerid == PLAYERONE && k == x && l+1 == y) return MELLETTE;
    if(playerid == PLAYERONE && k-1 == x && l+1 == y) return ATLOBAN;
    if(playerid == PLAYERONE && k+1 == x && l+1 == y) return ATLOBAN;

    if(playerid == PLAYERTWO && k == x && l-1 == y) return MELLETTE;
    if(playerid == PLAYERTWO && k+1 == x && l-1 == y) return ATLOBAN;
    if(playerid == PLAYERTWO && k-1 == x && l-1 == y) return ATLOBAN;
    return HIBAS;
}
void beadando::CheckWin(const int id)
{
    msgBox.setText(QString(trUtf8("A játék véget ért. Nyertes a %1. játékos.")).arg(id));
    msgBox.exec();
    StopGame();
    StartGame();
}
void beadando::NewGameButtonClicked()
{
    StartGame();
}
void beadando::ButtonClicked()
{
    QPushButton* senderButton = dynamic_cast <QPushButton*> (QObject::sender());
    int location = gombtar->indexOf(senderButton);
    int x = location / 10;
    int y = location % 10;

    StepGame(x,y);
    UpdateGameView();
}
void beadando::UpdateGameView()
{
    for(int i=0; i<10; ++i)
    {
        for(int j=0; j<10; ++j)
        {
            if(gamedata[i][j] == 0)
            {
                gombdata[i][j]->setText(trUtf8(""));
                gombdata[i][j]->setStyleSheet("");
            }
            else
            {
                gombdata[i][j]->setText(QString::number(gamedata[i][j]));
                if(playerid != gombdata[i][j]->text().toInt())
                {
                    gombdata[i][j]->setStyleSheet("");
                }
                else
                {
                    gombdata[i][j]->setStyleSheet("background-color: lightgreen");
                }
            }
        }
    }
    if(vankijelolt)
    {
        gombdata[kijelolt[0]][kijelolt[1]]->setStyleSheet("background-color: red");
    }
    else
    {
        if(playerid == gombdata[kijelolt[0]][kijelolt[1]]->text().toInt())
        {
            gombdata[kijelolt[0]][kijelolt[1]]->setStyleSheet("background-color: lightgreen");
        } else gombdata[kijelolt[0]][kijelolt[1]]->setStyleSheet("");
    }
}
void beadando::ResetView()
{
    for(int i=0; i<10; i++)
    {
        for(int j=0; j<10; j++)
        {
            gamedata[i][j] = 0;
        }
    }
    UpdateGameView();
}
void beadando::HideButtons()
{
    for(int i=0; i<10; i++)
    {
        for(int j=0; j<10; j++)
        {
            gombdata[i][j]->hide();
        }
    }
}

